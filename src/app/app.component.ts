import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  displayValue="";
  getName(name: string){
    alert(name);
  };
  counter = 0;
  inc(){
    this.counter++;
  }
  dec(){
    this.counter--;
  }
  getValue(value: string){
    console.log(value);
    this.displayValue = value;
  }
  
}
